﻿using System;
using System.Threading.Tasks;
using BackendNetCore.Core.ViewModels;
using BackendNetCore.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BackendNetCore.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationUserController : ControllerBase
    {
        private readonly IUserService _userService;
        public ApplicationUserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("Register")]
        public async Task<IActionResult> CreateUserAsync([FromBody] ApplicationUserViewModel entity)
        {
            try
            {
               var result = await _userService.CreateUserAsync(entity);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }
        [HttpPost("Login")]
        public async Task<IActionResult> LoginAsync([FromBody] LoginUserViewModel entity)
        {
            try
            {
                var token = await _userService.LoginUserAsync(entity);
                if(token!=null)
                    return Ok(new {token});
                else
                    return BadRequest(new { message = "Username or password is incorrect." });
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = "Interval server error" });
            }
        }
    }
}