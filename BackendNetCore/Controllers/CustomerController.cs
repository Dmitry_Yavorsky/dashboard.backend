﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackendNetCore.Core.Entities;
using BackendNetCore.Infrastructure.Contexts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BackendNetCore.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CustomerController : ControllerBase
    {
        private readonly ApiContext _context;

        public CustomerController(ApiContext context)
        {
            _context = context;
        }
        [HttpGet]
        public IActionResult Get()
        {
            var data = _context.Customers.OrderBy(c => c.Id);
            return Ok(data);
        }
        [HttpGet("{id}",Name = "GetCusomer")]
        public IActionResult Get(int id)
        {
            var data = _context.Customers.Find(id);
            return Ok(data);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Customer customer)
        {
            if (customer == null)
                return BadRequest();
            _context.Add(customer);
            _context.SaveChanges();
            return CreatedAtRoute("GetCustomer", new {id = customer.Id}, customer);
        }
    }
}