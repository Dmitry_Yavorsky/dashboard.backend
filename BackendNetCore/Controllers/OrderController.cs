﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackendNetCore.Api.Helpers;
using BackendNetCore.Core.Entities;
using BackendNetCore.Core.ViewModels;
using BackendNetCore.Infrastructure.Contexts;
using BackendNetCore.Infrastructure.Extensions;
using BackendNetCore.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace BackendNetCore.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
   
    public class OrderController : ControllerBase
    {
        private readonly ApiContext _context;
        
        private IOrderService _orderService;
        private IUrlHelper _urlHelper;
        public OrderController(ApiContext context,IOrderService orderService,IUrlHelper urlHelper)
        {
            _orderService = orderService;
            _context = context;
            _urlHelper = urlHelper;

        }

        [HttpGet("{pageIndex:int}/{pageSize:int}")]
        public IActionResult Get(int pageIndex, int pageSize)
        {
            var data = _context.Orders.Include(o => o.Customer).OrderByDescending(c => c.Placed);
            var page = new PaginatedResponse<Order>(data,pageIndex,pageSize);
            var totalCount = data.Count();
            var totalPages = Math.Ceiling((double) totalCount / pageSize);
            var response = new
            {
                Page = page,
                TotalPages = totalPages
            };
            return Ok(response);
        }
        //[HttpGet]
        //public IActionResult GetAll()
        //{
        //    var data = _orderService.GetAll();
        //    return Ok(data);
        //}
        [HttpGet("ByState")]
        public IActionResult ByState()
        {
            var orders = _context.Orders.Include(e => e.Customer).ToList();

            var groupedResult = orders.GroupBy(o => o.Customer.State).ToList().Select(grp=>new
            {
                State =grp.Key,
                Total = grp.Sum(x=>x.Total)
            }).OrderByDescending(e=>e.Total).ToList();
            return Ok(groupedResult);
        }
        [HttpGet("ByCustomer/{n}")]
        public IActionResult ByCustomer(int n)
        {
            var orders = _context.Orders.Include(e => e.Customer).ToList();

            var groupedResult = orders.GroupBy(o => o.Customer.Id).ToList().Select(grp => new
            {
                Name = _context.Customers.Find(grp.Key).Name,
                Total = grp.Sum(x => x.Total)
            }).OrderByDescending(e => e.Total).Take(n).ToList();
            return Ok(groupedResult);
        }

        [HttpGet("GetOrder/{id}", Name = "GetOrder")]
        public IActionResult GetOrder(int id)
        {
            var order = _context.Orders.Include(o => o.Customer).First(o => o.Id == id);
            return Ok(order);
        }

        [HttpGet(Name = nameof(GetAll))]
        public async Task<IActionResult> GetAll([FromQuery] QueryParameters paramters)
        {
            try
            {
                var allOrders = await _orderService.GetAllAsync(paramters);
                var allItemsCount = _orderService.Count();
                var paginationMetadata = new
                {
                    totalCount = allItemsCount,
                    pageSize = paramters.PageCount,
                    currentPage = paramters.Page,
                    totalPages = paramters.GetTotalPages(paramters.PageCount)
                };
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));
                var links = CreateLinksForCollection(paramters, allItemsCount);

                //var toReturn = allOrders.Select(x=>x.)
                return Ok(new
                {
                    value = allOrders,
                    links
                });

            }
            catch (Exception e)
            {
                return BadRequest();
            }
           

        }
        private List<LinkDto> CreateLinksForCollection(QueryParameters queryParameters, int totalCount)
        {
            var links = new List<LinkDto>();

           

            // self 
            links.Add(
             new LinkDto(_urlHelper.Link(nameof(GetAll), new
             {
                 pagecount = queryParameters.PageCount,
                 page = queryParameters.Page,
                 orderby = queryParameters.OrderBy
             }), "self", "GET"));

            links.Add(new LinkDto(_urlHelper.Link(nameof(GetAll), new
            {
                pagecount = queryParameters.PageCount,
                page = 1,
                orderby = queryParameters.OrderBy
            }), "first", "GET"));

            links.Add(new LinkDto(_urlHelper.Link(nameof(GetAll), new
            {
                pagecount = queryParameters.PageCount,
                page = queryParameters.GetTotalPages(totalCount),
                orderby = queryParameters.OrderBy
            }), "last", "GET"));

            if (queryParameters.HasNext(totalCount))
            {
                links.Add(new LinkDto(_urlHelper.Link(nameof(GetAll), new
                {
                    pagecount = queryParameters.PageCount,
                    page = queryParameters.Page + 1,
                    orderby = queryParameters.OrderBy
                }), "next", "GET"));
            }

            if (queryParameters.HasPrevious())
            {
                links.Add(new LinkDto(_urlHelper.Link(nameof(GetAll), new
                {
                    pagecount = queryParameters.PageCount,
                    page = queryParameters.Page - 1,
                    orderby = queryParameters.OrderBy
                }), "previous", "GET"));
            }

            return links;
        }

        //private dynamic ExpandSingleItem(Order order)
        //{
        //    var links = GetLinks(customer.Id);
        //    CustomerDto item = Mapper.Map<CustomerDto>(customer);

        //    var resourceToReturn = item.ToDynamic() as IDictionary<string, object>;
        //    resourceToReturn.Add("links", links);

        //    return resourceToReturn;
        //}
    }
}