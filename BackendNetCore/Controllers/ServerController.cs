﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackendNetCore.Api.Helpers;
using BackendNetCore.Api.SignalR;
using BackendNetCore.Infrastructure.Contexts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace BackendNetCore.Api.Controllers
{
    [Route("api/[controller]")]
    
    public class ServerController : ControllerBase
    {
        private readonly ApiContext _context;
        public ServerController(ApiContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var server = _context.Servers.OrderBy(s=>s.Name).ToList();
            return Ok(server);
        }

        [HttpGet("{id}", Name = "GetServer")]
        public IActionResult Get(int id)
        {
            var server = _context.Servers.Find(id);
            return Ok(server);
        }
        [HttpPut("{id}")]
        public IActionResult Put(int id,[FromBody] ServerMessage msg)
        {
            var server = _context.Servers.Find(id);
             
            if (server == null)
            {
                return NotFound();
            }

            if (msg.Payload == "activate")
            {
                server.IsOnline = true;
            }

            if (msg.Payload == "deactivate")
            {
                server.IsOnline = false;
            }
            _context.SaveChanges();
            return Ok(new {message = "Server has been succesfully updated"});
        }
    }
}
