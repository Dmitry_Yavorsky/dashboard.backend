﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendNetCore.Api.Helpers
{
    public class ServerMessage
    {
        public int Id { get; set; }
        public string Payload { get; set; }
    }
}
