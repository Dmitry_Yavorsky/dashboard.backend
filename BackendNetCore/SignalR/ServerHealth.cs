﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackendNetCore.Api.Helpers;
using BackendNetCore.Core.ViewModels;
using BackendNetCore.Infrastructure.Contexts;
using Microsoft.AspNetCore.SignalR;
using Microsoft.IdentityModel.Tokens;
using Remotion.Linq.Parsing.Structure.IntermediateModel;

namespace BackendNetCore.Api.SignalR
{
    public class ServerHealth: Hub
    {
        private readonly ApiContext _context;
        public ServerHealth(ApiContext context)
        {
            _context = context;
        }
        public async Task SendAsync(ServerMessage msg)
        {
            var server = _context.Servers.Find(msg.Id);

            if (server != null)
            {
            if (msg.Payload == "activate")
            {
                server.IsOnline = true;
            }

            if (msg.Payload == "deactivate")
            {
                server.IsOnline = false;
            }
            _context.SaveChanges();
            await Clients.All.SendAsync("serverHealth", new ResultOperationMessage {Id = server.Id, isSucceeded = true});
            }
            else
            {
            await Clients.All.SendAsync("serverHealth", new ResultOperationMessage { Id = server.Id, isSucceeded = false});
            }
        }
    }
}
