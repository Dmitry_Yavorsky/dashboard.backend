﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BackendNetCore.Api.SignalR;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BackendNetCore.Api.Shedulers
{
    public class SchedulerHostedService : HostedServiceBase
    {
        private readonly Random _random = new Random();
        private readonly IOptions<TimerServiceConfiguration> _options;
        private readonly ILogger<SchedulerHostedService> _logger;
        private readonly IHubContext<ServerHealth> _serverHealthContext;
        
        public SchedulerHostedService(ILoggerFactory loggerFactory, IHubContext<ServerHealth> hubContext, IOptions<TimerServiceConfiguration> configuration)
        {
            _logger = loggerFactory.CreateLogger<SchedulerHostedService>();
            _serverHealthContext = hubContext;
            _options = configuration;

        }
        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            //asdasd
            while (!cancellationToken.IsCancellationRequested)
            {
                //
                var randomValue = _random.Next(0, 100);

                _logger.LogInformation($"Sending information {randomValue}");


                await _serverHealthContext.Clients.All.SendAsync("newCpuValue", randomValue);

                await Task.Delay(TimeSpan.FromMilliseconds(_options.Value.Period), cancellationToken);


            }
        }
    }
}
