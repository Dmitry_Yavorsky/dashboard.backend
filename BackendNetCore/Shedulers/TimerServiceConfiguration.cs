﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendNetCore.Api.Shedulers
{
    public class TimerServiceConfiguration
    {
        public int DueTime { get; set; }

        public int Period { get; set; }
    }
}
