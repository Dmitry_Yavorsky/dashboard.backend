﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;


namespace BackendNetCore.Core.Entities.Identity
{
    public class ApplicationUser :  IdentityUser
    {
        public string FullName { get; set; }
        public int Age { get; set; }
    }
}
