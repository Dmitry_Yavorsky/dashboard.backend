﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendNetCore.Core.ViewModels
{
    public class LoginUserViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
