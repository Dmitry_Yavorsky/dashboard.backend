﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendNetCore.Core.ViewModels
{
    public class ResultOperationMessage
    {
        public int Id { get; set; }
        public bool isSucceeded { get; set; }
    }
}
