﻿using BackendNetCore.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace BackendNetCore.Infrastructure.Contexts
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options) : base(options)
        {
            
        }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Server> Servers { get; set; }
    }
}
