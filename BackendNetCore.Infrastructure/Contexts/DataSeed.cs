﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using BackendNetCore.Core.Entities;
using BackendNetCore.Infrastructure.Helpers;
using Microsoft.EntityFrameworkCore.Internal;

namespace BackendNetCore.Infrastructure.Contexts
{
    public class DataSeed
    {
        private readonly ApiContext _context;
        public DataSeed(ApiContext context)
        {
            _context = context;
        }

        public void SeedData(int nCustomers,int nOrders)
        {
            if (!_context.Customers.Any())
            {
                SeedCustomers(nCustomers);
                _context.SaveChanges();
            }

            if (!_context.Orders.Any())
            {
                SeedOrders(nOrders);
                _context.SaveChanges();
            }

            if (!_context.Servers.Any())
            {
                SeedServers();
                _context.SaveChanges();
            }
        }

        private void SeedServers()
        {
            List<Server> servers = BuildServersList();
            foreach (var server in servers)
            {
                _context.Servers.Add(server);
            }
        }
        private void SeedOrders(int nOrders)
        {
            List<Order> orders = BuildOrdersList(nOrders);
            foreach (var order in orders)
            {
                _context.Orders.Add(order);
            }
        }
        private void SeedCustomers(int nCustomers)
        {
            List<Customer> customers = BuildCustomersList(nCustomers);
            foreach (var customer in customers)
            {
                _context.Customers.Add(customer);
            }
        }

        private List<Customer> BuildCustomersList(int nCustomers)
        {
            var customers = new List<Customer>();
            var names = new List<string>();
            for (int i = 1; i < nCustomers; i++)
            {
                var name = Generator.MakeUniqueCustomerName(names);
                names.Add(name);
                customers.Add(new Customer()
                {
                    Id = i,
                    Name = name,
                    Email = Generator.MakeCustomerEmail(name),
                    State = Generator.GetRandomState()
                });
            }

            return customers;
        }

        private List<Order> BuildOrdersList(int nOrders)
        {
            var orders = new List<Order>();
            var rand = new Random();

            for (int i = 1; i < nOrders; i++)
            {
                var randCustomerId = rand.Next(1,_context.Customers.Count());
            var placed = Generator.GetRandomOrderPlaced();
            var completed = Generator.GetRandomOrderCompleted(placed);
               orders.Add(new Order
               {
                   Id = i,
                   Customer = _context.Customers.First(c=>c.Id==randCustomerId),
                   Total = Generator.GetRandomOrderTotal(),
                   Placed = placed,
                   Completed = completed
               });
            }

            return orders;
        }

        private List<Server> BuildServersList()
        {
           return new List<Server>
            {
                new Server()
                {
                    Id = 1,
                    Name = "Dev-web",
                    IsOnline = true,
                },
                new Server()
                {
                    Id = 2,
                    Name = "Dev-Mail",
                    IsOnline = false,
                },
                new Server()
                {
                    Id = 3,
                    Name = "Dev-Services",
                    IsOnline = true,
                },
                new Server()
                {
                    Id = 4,
                    Name = "QA-Web",
                    IsOnline = true,
                },
                new Server()
                {
                    Id = 5,
                    Name = "QA-mail",
                    IsOnline = true,
                },
                new Server()
                {
                    Id = 6,
                    Name = "QA-Services",
                    IsOnline = true,
                },
                new Server()
                {
                    Id = 7,
                    Name = "Prod-Services",
                    IsOnline = true,
                },
                new Server()
                {
                    Id = 8,
                    Name = "Prod-Services",
                    IsOnline = true,
                },
                new Server()
                {
                    Id = 9,
                    Name = "Prod-Services",
                    IsOnline = true,
                },
            };
        }

    }
}
