﻿
using BackendNetCore.Core.Entities.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BackendNetCore.Infrastructure.Contexts
{
    public class IdentityContext  : IdentityDbContext
    {
        public IdentityContext(DbContextOptions options):base(options)
        {
            
        }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
    }
}
