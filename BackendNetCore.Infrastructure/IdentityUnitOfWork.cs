﻿using System;
using System.Collections.Generic;
using System.Text;
using BackendNetCore.Core.Entities.Identity;
using BackendNetCore.Infrastructure.Contexts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace BackendNetCore.Infrastructure
{
    public class IdentityUnitOfWork
    {
        private UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;
        private readonly IdentityContext _identityContext;
        private readonly ApiContext _apiContext;
        public IdentityUnitOfWork(IdentityContext identityContext,UserManager<ApplicationUser> userManager,SignInManager<ApplicationUser> signInManager)
        {
            _identityContext = identityContext;
            _userManager = userManager;
            _signInManager = signInManager;

        }

        public UserManager<ApplicationUser> UserManager
        {
            get
            {
               
                return _userManager;
            }
        }
        public SignInManager<ApplicationUser> SignInManager
        {
            get
            {
                return _signInManager;
            }
        } 
    }
}
