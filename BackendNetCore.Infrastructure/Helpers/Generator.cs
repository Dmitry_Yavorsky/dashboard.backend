﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendNetCore.Infrastructure.Helpers
{
    public class Generator
    {
        private static Random _rnd = new Random();
        private static string GetRandom(IList<string> items)
        {
            return items[_rnd.Next(items.Count)];

        }
        internal static string MakeUniqueCustomerName(List<string> names)
        {
            var maxNames = bizPreffix.Count * bizSuffix.Count;
            if (names.Count >= maxNames)
            {
                throw new InvalidOperationException("Maximum number of uniquet names exceeded");
            }
            var prefix = GetRandom(bizPreffix);
            var suffix = GetRandom(bizSuffix);
            var fullname = prefix + suffix;
            if (names.Contains(fullname))
                MakeUniqueCustomerName(names);
            return fullname;
        }

        internal static string MakeCustomerEmail(string name)
        {
            return $"{name.ToLower()}.com";
        }

        internal static string GetRandomState()
        {
            return GetRandom(usStates);
        }

       
        internal static DateTime GetRandomOrderPlaced()
        {
            var end = DateTime.Now;
            var start = end.AddDays(-90);
            TimeSpan possibleTimeSpan = end - start;
            TimeSpan newSpan = new TimeSpan(0,_rnd.Next(0,(int)possibleTimeSpan.TotalMinutes),0);    
            return start + newSpan;
        }
        internal static DateTime? GetRandomOrderCompleted(DateTime orderPlaced)
        {
            var now = DateTime.Now;
            var minLeadTime = TimeSpan.FromDays(7);
            var timePassed = now - orderPlaced;
            if (timePassed < minLeadTime)
            {
                return null;
            }

            return orderPlaced.AddDays(_rnd.Next(7, 14));
        }

        internal static decimal GetRandomOrderTotal()
        {
            return _rnd.Next(100, 5000);
        }
        private static readonly List<string> usStates = new List<string>()
        {
            "Alabama - AL",
            "Alaska - AK",
            "Arizona - AZ",
            "Arkansas - AR",
            "California - CA",
            "Colorado - CO",
            "Connecticut - CT",
            "Delaware - DE",
            "Florida - FL",
            "Georgia - GA",
            "Hawaii - HI",
            "Idaho - ID",
            "Illinois - IL",
            "Indiana - IN",
            "Iowa - IA",
            "Kansas - KS",
            "Kentucky - KY",
            "Louisiana - LA",
            "Maine - ME",
            "Maryland - MD",
            "Massachusetts - MA",
            "Michigan - MI",
            "Minnesota - MN",
            "Mississippi - MS",
            "Missouri - MO",
            "Montana - MT",
            "Nebraska - NE",
            "Nevada - NV",
            "New Hampshire - NH",
            "New Jersey - NJ",
            "New Mexico - NM",
            "New York - NY",
            "North Carolina - NC",
            "North Dakota - ND",
            "Ohio - OH",
            "Oklahoma - OK",
           
        };
        private static readonly List<string> bizPreffix =new List<string>()
        {
            "ABC",
            "XYZ",
            "MainSt",
            "Sales",
            "Enterprise",
            "Ready",
            "Quick",
            "Budget",
            "Magic",

        };
        private static readonly List<string> bizSuffix = new List<string>()
        {
            "Coorporation",
            "Entertaiment",
            "Bakkery",
            "Dog",
            "Hotels",
            "Ready",
            "Quick",
            "Budget",
            "Magic",

        };
    }
}
