﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BackendNetCore.Core.Entities;
using BackendNetCore.Core.ViewModels;

namespace BackendNetCore.Infrastructure.Repositories.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
        Task<IEnumerable<Order>> GetAllAsync(QueryParameters parameters);
    }
}
