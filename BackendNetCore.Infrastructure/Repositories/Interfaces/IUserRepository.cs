﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BackendNetCore.Core.Entities.Identity;

namespace BackendNetCore.Infrastructure.Repositories.Interfaces
{
    public interface IUserRepository
    {
        Task CreateUser(ApplicationUser entity);
    }
}
