﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackendNetCore.Core.Entities;
using BackendNetCore.Core.ViewModels;
using BackendNetCore.Infrastructure.Contexts;
using BackendNetCore.Infrastructure.Extensions;
using BackendNetCore.Infrastructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace BackendNetCore.Infrastructure.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(ApiContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Order>> GetAllAsync(QueryParameters parameters)
        {
            var allItems = await _entities.Include(c=>c.Customer)
                .Skip(parameters.PageCount * (parameters.Page - 1)).Take(parameters.PageCount).OrderBy(parameters.OrderBy,parameters.IsDescending()).ToListAsync();
            return allItems;

        }

        public override IEnumerable<Order> GetAll()
        {
            return _entities.Include(e => e.Customer);
        }
    }
}
