﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BackendNetCore.Core.ViewModels;

namespace BackendNetCore.Infrastructure.Extensions
{
   public static class QueryParametersExtensions
    {
        public static bool HasPrevious(this QueryParameters parameters)
        {
            return parameters.Page > 1;
        }
        public static bool IsDescending(this QueryParameters parameters)
        {
            if (!string.IsNullOrEmpty(parameters.OrderBy))
            {
                return parameters.OrderBy.Split(' ').Last().ToLowerInvariant().StartsWith("desc");
            }

            return false;
        }

        public static bool HasQuery(this QueryParameters parameters)
        {
            return !String.IsNullOrEmpty(parameters.Query);
        }
        public static double GetTotalPages(this QueryParameters queryParameters, int totalCount)
        {
            return Math.Ceiling(totalCount / (double)queryParameters.PageCount);
        }

        public static bool HasNext(this QueryParameters parameters,int totalCount)
        {
            return (parameters.Page < (int) GetTotalPages(parameters, totalCount));
        }
    }
}
