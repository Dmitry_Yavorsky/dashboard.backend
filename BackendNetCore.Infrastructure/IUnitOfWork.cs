﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BackendNetCore.Infrastructure.Repositories.Interfaces;

namespace BackendNetCore.Infrastructure
{
    public interface IUnitOfWork
    {
        IOrderRepository Orders { get; }
        int SaveChanges();
    }
}
