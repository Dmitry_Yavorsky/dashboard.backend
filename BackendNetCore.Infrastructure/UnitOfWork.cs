﻿using System;
using System.Collections.Generic;
using System.Text;
using BackendNetCore.Infrastructure.Contexts;
using BackendNetCore.Infrastructure.Repositories;
using BackendNetCore.Infrastructure.Repositories.Interfaces;

namespace BackendNetCore.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApiContext _context;
        public UnitOfWork(ApiContext context)
        {
            _context = context;
        }
        private IOrderRepository _orders;

        public IOrderRepository Orders
        {
            get
            {
                if(_orders==null)
                    _orders = new OrderRepository(_context);
                return _orders;
            }
        }
        public int SaveChanges()
        {
           return _context.SaveChanges();
        }
    }
}
