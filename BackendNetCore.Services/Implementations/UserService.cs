﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BackendNetCore.Core.Entities.Identity;
using BackendNetCore.Core.ViewModels;
using BackendNetCore.Infrastructure;
using BackendNetCore.Services.Interfaces;

using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;

namespace BackendNetCore.Services.Implementations
{
    public class UserService : IUserService
    {
        private readonly IdentityUnitOfWork _uow;
        private readonly IMapper _mapper;
        private readonly ApplicationSettings _appSettings;
        public UserService(IdentityUnitOfWork uow,IMapper mapper,IOptions<ApplicationSettings> applicationSettings)
        {
            _uow = uow;
            _mapper = mapper;
            _appSettings = applicationSettings.Value;

        }
        public async Task<IdentityResult> CreateUserAsync(ApplicationUserViewModel entity)
        {
            var user = _mapper.Map<ApplicationUserViewModel, ApplicationUser>(entity);
            var result = await _uow.UserManager.CreateAsync(user,entity.Password);
            return result;
        }

        public async Task<string> LoginUserAsync(LoginUserViewModel entity)
        {
            var user = await _uow.UserManager.FindByNameAsync(entity.UserName);
            if (user != null && await _uow.UserManager.CheckPasswordAsync(user, entity.Password))
            {
                var tokenDescription = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim("UserId", user.Id.ToString())
                    }),
                    Expires = DateTime.UtcNow.AddDays(1),
                    SigningCredentials = new SigningCredentials(
                        new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.JWT_Secret)),
                        SecurityAlgorithms.HmacSha256Signature)

                };
                var tokenHandler = new JwtSecurityTokenHandler();
                var securityToken = tokenHandler.CreateToken(tokenDescription);
                var token = tokenHandler.WriteToken(securityToken);
                return token;
            }

            return null;
        }
    }
}
