﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackendNetCore.Core.Entities;
using BackendNetCore.Core.ViewModels;
using BackendNetCore.Infrastructure;
using BackendNetCore.Services.Interfaces;

namespace BackendNetCore.Services.Implementations
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _uow;
        public OrderService(IUnitOfWork uow)
        {
            _uow = uow;
        }
        public async Task<IEnumerable<Order>> GetAllAsync(QueryParameters parameters)
        {
            return await _uow.Orders.GetAllAsync(parameters);
        }

        public int Count()
        {
            return _uow.Orders.Count();
        }

        public List<Order> GetAll()
        {
            return  _uow.Orders.GetAll().ToList();
        }
    }
}
