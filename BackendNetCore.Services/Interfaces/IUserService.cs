﻿using System.Threading.Tasks;
using BackendNetCore.Core.ViewModels;

using Microsoft.AspNetCore.Identity;

namespace BackendNetCore.Services.Interfaces
{
    public interface IUserService
    {
        Task<IdentityResult> CreateUserAsync(ApplicationUserViewModel entity);
        Task<string> LoginUserAsync(LoginUserViewModel entity);
    }
}
