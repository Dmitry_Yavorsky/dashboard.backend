﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BackendNetCore.Core.Entities;
using BackendNetCore.Core.ViewModels;

namespace BackendNetCore.Services.Interfaces
{
    public interface IOrderService
    {
        Task<IEnumerable<Order>> GetAllAsync(QueryParameters parameters);
        int Count();
        List<Order> GetAll();
    }
}
