﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using BackendNetCore.Core.Entities.Identity;
using BackendNetCore.Core.ViewModels;

namespace BackendNetCore.Services.Mapper
{
    public class MapperProfile: Profile
    {
        public MapperProfile()
        {
            CreateMap<ApplicationUserViewModel, ApplicationUser>();
        }
    }
}
